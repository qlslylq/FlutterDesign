import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Parcel.dart';

/*
 *  列表控件基类<br/>
 */
abstract class BaseListView<T> extends StatefulWidget {

  final List<T> list;

  const BaseListView({Key key, @required this.list}) : super(key: key);
}

/*
 *  列表控件状态基类<br/>
 */
abstract class BaseListViewState extends State<BaseListView> {

  Widget build(BuildContext context) {
    List<ListTile> list_tile = widget.list.map((item) {
      return createListTile(item);
    }).toList();

    List<Widget> list_widget =
    ListTile.divideTiles(context: context, tiles: list_tile).toList();

    return new ListView(children: list_widget);
  }

  ListTile createListTile(dynamic item);
}
