/*
 * App常用函数
 */
import 'package:flutter_design/com/qlslylq/flutterdesign/util/TextUtils.dart';

class AppMethod {

  /*
   * 手机信息
   */
  static String _userAgent;

  /*
   * 获取手机信息
   */
  static String getUserAgent() {
    if (TextUtils.isEmpty(_userAgent)) {
      StringBuffer ua = new StringBuffer("FlutterDesign_1.0");
//      PackageInfo appInfo = AppMethod.getPackageInfo();
//      if (appInfo != null) {
//        ua.append("/").append(appInfo.versionName).append("_")
//            .append(appInfo.versionCode);
//      }
//      ua.append("/android ").append(android.os.Build.VERSION.RELEASE);
//      ua.append("/").append(android.os.Build.MODEL);
//      ua.append("/").append(AppMethod.getAppId());
      _userAgent = ua.toString();
    }
    return _userAgent;
  }
}