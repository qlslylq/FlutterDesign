// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Parcel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Parcel _$ParcelFromJson(Map<String, dynamic> json) {
  return Parcel()
    ..time = json['time'] as String
    ..ftime = json['ftime'] as String
    ..context = json['context'] as String
    ..location = json['location'] as String;
}

Map<String, dynamic> _$ParcelToJson(Parcel instance) => <String, dynamic>{
      'time': instance.time,
      'ftime': instance.ftime,
      'context': instance.context,
      'location': instance.location
    };
