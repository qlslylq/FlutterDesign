import 'package:flutter_design/com/qlslylq/flutterdesign/bean/AdmxObject.dart';

/*
 * 用户信息<br/>
 */
class AdmxUser extends AdmxObject {

  String account; //登录账号，同步至SP

  String password; //登录密码，同步至SP

  String mobile; //手机号，错误邮件记录


}
